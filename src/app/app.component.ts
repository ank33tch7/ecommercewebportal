import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { trigger, transition, style, animate } from '@angular/animations';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('flyInOut', [
      transition(':enter', [
        style({ opacity: 0, transform: 'translateY(-100%)' }),
        animate('500ms', style({ opacity: 1, transform: 'translateY(0)' }))
      ]),
      transition(':leave', [
        animate('500ms', style({ opacity: 0, transform: 'translateY(-100%)' }))
      ])
    ])
  ]
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'EcommercePortal';

  routerSubscription: Subscription;
  constructor(private _router: Router) {

  }
  ngOnInit(): void {
    //  this.routerSubscription = this._router.events.subscribe((evt) => {
    //     // if (!(evt instanceof NavigationEnd)) {
    //     //   return;
    //     // }
    //     // window.scrollTo(0, 0);
    //     // this._router.routeReuseStrategy.shouldReuseRoute = () => false;
    //   }
    //   );
  }
  onActive() {
    window.scroll(0, 0);
  }
  ngOnDestroy(): void {
    this.routerSubscription.unsubscribe();
  }
}
