import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CustomerOrderApiService } from 'src/app/services/web-api-client';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css']
})
export class OrderHistoryComponent implements OnInit, OnDestroy {

  orderHistoryDetails : any;

  orderHistorySubscription : Subscription;

  constructor(private _orderService: CustomerOrderApiService) {}



  ngOnInit(): void {
      this.loadOrderHistory();
  }

  loadOrderHistory() : void {
    this.orderHistorySubscription = this._orderService.getOrderHistory().subscribe(orderHistory =>{
      this.orderHistoryDetails = orderHistory;
    });
  }

  ngOnDestroy(): void {
    if(this.orderHistorySubscription){
      this.orderHistorySubscription.unsubscribe();
    }
  }
}
