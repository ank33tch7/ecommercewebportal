import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopPromoComponent } from './shop-promo.component';

describe('ShopPromoComponent', () => {
  let component: ShopPromoComponent;
  let fixture: ComponentFixture<ShopPromoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ShopPromoComponent]
    });
    fixture = TestBed.createComponent(ShopPromoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
