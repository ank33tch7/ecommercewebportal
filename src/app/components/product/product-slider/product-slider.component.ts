import {  AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CustomerPortalApiService, ImageSliderDTO } from 'src/app/services/web-api-client';
import { environment } from 'src/app/environments/environment';
declare var $: any;

@Component({
  selector: 'app-product-slider',
  templateUrl: './product-slider.component.html',
  styleUrls: ['./product-slider.component.css']
})
export class ProductSliderComponent implements OnInit, AfterViewInit,OnDestroy {
  sliders: ImageSliderDTO[] = [];
  // images: any[] = [];
  sliderSubscription: Subscription;
  bxSliderInstance: any;

  // @ViewChild('carousel') carousel: ElementRef;
  readonly baseUrl = environment.imageUrl;
  constructor(private _sliderService: CustomerPortalApiService) { 
            }

  ngOnInit(): void {
    this.loadSliderData();
    
  }

  ngAfterViewInit(): void {

  }

  loadSliderData(): void {
    this.sliderSubscription = this._sliderService.getImageSlider().subscribe((sliders: ImageSliderDTO[]) => {
      this.sliders = sliders;
      // this.images = sliders.map(slider => {
      //   return {
      //     imageBase64: `data:image/${slider?.image?.imageExtension};base64,${slider?.image?.imageBase64}`,
      //   };
      // });
      if (this.bxSliderInstance) {
        this.bxSliderInstance.reloadSlider();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.sliderSubscription) {
      this.sliderSubscription.unsubscribe();
    }
  }

  carouselConfig: any = {
    dots: true,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };


}
