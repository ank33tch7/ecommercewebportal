import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LastestProductComponent } from './lastest-product.component';

describe('LastestProductComponent', () => {
  let component: LastestProductComponent;
  let fixture: ComponentFixture<LastestProductComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LastestProductComponent]
    });
    fixture = TestBed.createComponent(LastestProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
