import { Component, OnDestroy, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { CustomerPortalApiService } from 'src/app/services/web-api-client';
import { environment } from 'src/app/environments/environment';
@Component({
  selector: 'app-lastest-product',
  templateUrl: './lastest-product.component.html',
  styleUrls: ['./lastest-product.component.css']
})
export class LastestProductComponent implements OnInit,OnDestroy  {

  readonly baseUrl = environment.imageUrl;

  productData: any;
  private dataSubscription: Subscription;
  
  constructor(private _customerPortalService: CustomerPortalApiService,
              private _toastrService: ToastrService){}

  ngOnInit(): void {
    this.loadProductsData();
  }

  loadProductsData() : void{
    this.dataSubscription = this._customerPortalService.getProductList().subscribe(products =>{
      this.productData = products
    })
  }

  ngOnDestroy(): void {
    this.dataSubscription.unsubscribe();
  }
}
