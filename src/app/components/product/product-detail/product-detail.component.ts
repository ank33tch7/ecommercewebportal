import { Component, OnInit, AfterViewInit, OnDestroy, ElementRef, AfterViewChecked } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AddToCartModel } from 'src/app/models/add-to-cart.model';
import { AddToCartService } from 'src/app/services/add-to-cart.service';
import { BuyNowService } from 'src/app/services/buy-now-service';
import { OrderDetail } from 'src/app/services/order-detail';
import { CustomerPortalApiService, ShowProductDetailDTO } from 'src/app/services/web-api-client';
declare var $: any; // Import the global jQuery object

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit, AfterViewChecked, OnDestroy {

  id: any;
  productDetail: any;
  images: any = [];
  //for quantity
  quantity: number = 1;

  // cartdetails 
  currentIndex: number = 0;
  slideWidth: number = 0;


  private dataSubscription: Subscription;
  constructor(private _productService: CustomerPortalApiService,
    private route: ActivatedRoute,
    private _cartService: AddToCartService,
    private _toastrService: ToastrService,
    private elementRef: ElementRef,
    private _router: Router,
    private _buyNowService: BuyNowService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: any) => {
      this.id = params['id'];
      this.getProductById(this.id);
    });
  }

  getProductById(id: any) {
    this.dataSubscription = this._productService.getProductDetailById(id).subscribe((res: ShowProductDetailDTO) => {
      this.productDetail = res;
      this.images = res.images;
    }, err => {

    })
  }


  onAddQuantity(): void {
    if (!(this.quantity >= this.productDetail.quantity)) {
      this.quantity += 1;
    }

  }
  onSubtractQuantity(): void {
    if (this.quantity == 0) {
      this.quantity = 0;
    } else {
      this.quantity -= 1;
    }
  }

  onAddToCart(): void {
    if(this.productDetail?.quantity == 0){
      this._toastrService.info("Avaible Quantity cannot be zero.")
      return;
    }
    if (this.quantity == 0) {
      this._toastrService.info("Quantity cannot be zero.")
      return;
    }
    let cartModel = new AddToCartModel();
    cartModel.id = this.id;
    cartModel.brandName = this.productDetail?.brandName;
    cartModel.productName = this.productDetail?.productName;
    cartModel.quantity = this.quantity;
    cartModel.unitPrice = this.productDetail?.sellingUnitPrice;
    cartModel.totalAmount = this.productDetail?.sellingUnitPrice * this.quantity;

    if (this._cartService.checkIfExistsProductInCart(this.id)) {
      let quantity = this._cartService.getProductQuantityById(this.id);
      this._cartService.removeProduct(this.id);
      let newQuantity = this.quantity + quantity;
      cartModel.quantity = newQuantity
      cartModel.totalAmount = newQuantity * this.productDetail?.sellingUnitPrice;
    }

    this._cartService.onAddProductDetails(cartModel);
    this._cartService.onCountProduct();


  }
  // onAddToCartAndRoute(): void {
  //   this.onAddToCart();

  // }

  onBuyNow(){
    if(this.productDetail?.quantity == 0){
      this._toastrService.info("Avaible Quantity cannot be zero.")
      return;
    }
    let orderDetail = new OrderDetail();
    orderDetail.productId = this.id;
    orderDetail.price = this.productDetail?.sellingUnitPrice;
    orderDetail.quantity = this.quantity;
    this._buyNowService.onAddBuyProduct(orderDetail);
    const queryParams: NavigationExtras = {
      queryParams: { isBuyNow: true }
    };
    this._router.navigate(['/checkout'], queryParams);
  }

  nextSlide() {
    if (this.currentIndex < this.images?.length - 1) {
      this.currentIndex++;
    }
  }

  prevSlide() {
    if (this.currentIndex > 0) {
      this.currentIndex--;
    }
  }

  ngAfterViewChecked() {
    const slideElement = this.elementRef.nativeElement.querySelector('.slide');
    if (slideElement) {
      this.slideWidth = slideElement.clientWidth;

    }

  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}
