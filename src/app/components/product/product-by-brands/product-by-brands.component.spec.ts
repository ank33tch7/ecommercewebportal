import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductByBrandsComponent } from './product-by-brands.component';

describe('ProductByBrandsComponent', () => {
  let component: ProductByBrandsComponent;
  let fixture: ComponentFixture<ProductByBrandsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductByBrandsComponent]
    });
    fixture = TestBed.createComponent(ProductByBrandsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
