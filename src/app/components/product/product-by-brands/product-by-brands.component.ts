import { Component, OnDestroy, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { environment } from 'src/app/environments/environment';
import { CustomerPortalApiService } from 'src/app/services/web-api-client';

@Component({
  selector: 'app-product-by-brands',
  templateUrl: './product-by-brands.component.html',
  styleUrls: ['./product-by-brands.component.css']
})
export class ProductByBrandsComponent implements OnInit, OnDestroy{

  brandData : any;

  brandSubscription : Subscription;
  readonly baseUrl = environment.imageUrl;

  constructor(private _brandService: CustomerPortalApiService,
              private _toastrService: ToastrService){}

  ngOnInit(): void {
    this.loadBrandDetails();
  }

  loadBrandDetails(): void{
    this.brandSubscription = this._brandService.getBrandImages().subscribe(brandData  =>{
      this.brandData = brandData;
    },err =>{
      //this._toastrService.error("Data load failed");
    });
  }
  ngOnDestroy(): void {
    if(this.brandSubscription){
      this.brandSubscription.unsubscribe();
    }
  }

}
