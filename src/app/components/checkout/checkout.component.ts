import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { State } from 'src/app/models/constants/state';
import { District } from 'src/app/models/constants/district';
import { AddToCartService } from 'src/app/services/add-to-cart.service';
import { BuyNowService } from 'src/app/services/buy-now-service';
import { CustomerOrderApiService } from 'src/app/services/web-api-client';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit,OnDestroy {

  isBuyNow : boolean = false;
  // for place order form
  createOrderForm: any;
  submitted: boolean = false;

  orderSubscription: Subscription;
  constructor(private _formBuilder: FormBuilder,
              private _toastrService: ToastrService,
              private _orderService: CustomerOrderApiService,
              private _addToCartService: AddToCartService,
              private _router : Router,
              private _buyNowService: BuyNowService,
              private _route: ActivatedRoute,) {}

  ngOnInit(): void {
    this._route.queryParams.subscribe((params: any) => {
      this.isBuyNow = params['isBuyNow'];
    });
    let orderDetail = [];
    if(this.isBuyNow){
      orderDetail = this._buyNowService.getOrderDetail();
    }else{
      orderDetail = this._addToCartService.getOrderDetail();
    }
    this.createOrderForm = this._formBuilder.group({
      amount: [this._addToCartService.getTotalAmount(), Validators.required],
      state: [null, Validators.required],
      district: [null, Validators.required],
      municipality: [null, Validators.required],
      address: [null, Validators.required],
      zipCode: [null],
      fullName: [null, Validators.required],
      phone: [null, Validators.required],
      email: [null],
      orderDetails: [orderDetail]
    });

   
  }

  get getFormControl(){
    return this.createOrderForm.controls;
  }
 
  onCreateOrder() : void{
    this.submitted = true;
    if(this.createOrderForm.invalid) return;

    this.orderSubscription = this._orderService.createOrder(this.createOrderForm.value).subscribe(res =>{
      this._toastrService.success("Your order is created successfully.");
      this._addToCartService.removeAllProductFromCart();
      this._router.navigate(["/order-history"]);
    },err=>{
      this._toastrService.error("Order placed failed.");
    })

  }

  ngOnDestroy(): void {
    if(this.orderSubscription){
      this.orderSubscription.unsubscribe();
    }
  }


  states: any = [
    {id: State.KoshiPradesh, name: "Koshi Pradesh"},
    {id: State.MadeshPradesh, name: "Madesh Pradesh"},
    {id: State.BagmatiPradesh, name: "Bagmati Pradesh"},
    {id: State.GandakiPradesh, name: "Gandaki Pradesh"},
    {id: State.LumbiniPradesh, name: "Lumbini Pradesh"},
    {id: State.KarnaliPradesh, name: "Karnali Pradesh"},
    {id: State.SudurpaschimPradesh, name: "Sudurpaschim Pradesh"},
  ];

  districts : any = [
    // State 1: Koshi
    {id: District.Bhojpur, name: "Bhojpur", stateId: State.KoshiPradesh},
    {id: District.Dhankuta, name: "Dhankuta", stateId: State.KoshiPradesh},
    {id: District.Ilam, name: "Ilam", stateId: State.KoshiPradesh},
    {id: District.Jhapa, name: "Jhapa", stateId: State.KoshiPradesh},
    {id: District.Khotang, name: "Khotang", stateId: State.KoshiPradesh},
    {id: District.Morang, name: "Morang", stateId: State.KoshiPradesh},
    {id: District.Okhaldhunga, name: "Okhaldhunga", stateId: State.KoshiPradesh},
    {id: District.Sankhuwasabha, name: "Sankhuwasabha", stateId: State.KoshiPradesh},
    {id: District.Solukhumbu, name: "Solukhumbu", stateId: State.KoshiPradesh},
    {id: District.Sunsari, name: "Sunsari", stateId: State.KoshiPradesh},
    {id: District.Taplejung, name: "Taplejung", stateId: State.KoshiPradesh},
    {id: District.Terhathum, name: "Terhathum", stateId: State.KoshiPradesh},
    {id: District.Udayapur, name: "Udayapur", stateId: State.KoshiPradesh},
    
    // State 2: Madesh
    {id: District.Bara, name: "Bara", stateId: State.MadeshPradesh},
    {id: District.Dhanusha, name: "Dhanusha", stateId: State.MadeshPradesh},
    {id: District.Mahottari, name: "Mahottari", stateId: State.MadeshPradesh},
    {id: District.Parsa, name: "Parsa", stateId: State.MadeshPradesh},
    {id: District.Rautahat, name: "Rautahat", stateId: State.MadeshPradesh},
    {id: District.Saptari, name: "Saptari", stateId: State.MadeshPradesh},
    {id: District.Sarlahi, name: "Sarlahi", stateId: State.MadeshPradesh},
    {id: District.Siraha, name: "Siraha", stateId: State.MadeshPradesh},

    // State 3: Bagmati
    {id: District.Bhaktapur, name: "Bhaktapur", stateId: State.BagmatiPradesh},
    {id: District.Chitwan, name: "Chitwan", stateId: State.BagmatiPradesh},
    {id: District.Dhading, name: "Dhading", stateId: State.BagmatiPradesh},
    {id: District.Dolakha, name: "Dolakha", stateId: State.BagmatiPradesh},
    {id: District.Kathmandu, name: "Kathmandu", stateId: State.BagmatiPradesh},
    {id: District.Kavrepalanchowk, name: "Kavrepalanchok", stateId: State.BagmatiPradesh},
    {id: District.Lalitpur, name: "Lalitpur", stateId: State.BagmatiPradesh},
    {id: District.Makwanpur, name: "Makwanpur", stateId: State.BagmatiPradesh},
    {id: District.Nuwakot, name: "Nuwakot", stateId: State.BagmatiPradesh},
    {id: District.Ramechhap, name: "Ramechhap", stateId: State.BagmatiPradesh},
    {id: District.Rasuwa, name: "Rasuwa", stateId: State.BagmatiPradesh},
    {id: District.Sindhuli, name: "Sindhuli", stateId: State.BagmatiPradesh},
    {id: District.Sindhupalchowk, name: "Sindhupalchok", stateId: State.BagmatiPradesh},

     // State 4: Gandaki
     {id: District.Baglung, name: "Baglung", stateId: State.GandakiPradesh},
     {id: District.Gorkha, name: "Gorkha", stateId: State.GandakiPradesh},
     {id: District.Kaski, name: "Kaski", stateId: State.GandakiPradesh},
     {id: District.Lamjung, name: "Lamjung", stateId: State.GandakiPradesh},
     {id: District.Manang, name: "Manang", stateId: State.GandakiPradesh},
     {id: District.Mustang, name: "Mustang", stateId: State.GandakiPradesh},
     {id: District.Myagdi, name: "Myagdi", stateId: State.GandakiPradesh},
     {id: District.Nawalpur, name: "Nawalpur", stateId: State.GandakiPradesh},
     {id: District.Parbat, name: "Parbat", stateId: State.GandakiPradesh},
     {id: District.Syangja, name: "Syangja", stateId: State.GandakiPradesh},
     {id: District.Tanahun, name: "Tanahun", stateId: State.GandakiPradesh},

    // State 5: Lumbini
    {id: District.Arghakhanchi, name: "Arghakhanchi", stateId: State.LumbiniPradesh},
    {id: District.Banke, name: "Banke", stateId: State.LumbiniPradesh},
    {id: District.Bardiya, name: "Bardiya", stateId: State.LumbiniPradesh},
    {id: District.Dang, name: "Dang", stateId: State.LumbiniPradesh},
    {id: District.EasternRukum, name: "Eastern Rukum", stateId: State.LumbiniPradesh},
    {id: District.Gulmi, name: "Gulmi", stateId: State.LumbiniPradesh},
    {id: District.Kapilbastu, name: "Kapilvastu", stateId: State.LumbiniPradesh},
    {id: District.Palpa, name: "Palpa", stateId: State.LumbiniPradesh},
    {id: District.Parasi, name: "Parasi", stateId: State.LumbiniPradesh},
    {id: District.Pyuthan, name: "Pyuthan", stateId: State.LumbiniPradesh},
    {id: District.Rolpa, name: "Rolpa", stateId: State.LumbiniPradesh},
    {id: District.Rupandehi, name: "Rupandehi", stateId: State.LumbiniPradesh},

    // State 6: Lumbini
    {id: District.Dailekh, name: "Dailekh", stateId: State.KarnaliPradesh},
    {id: District.Dolpa, name: "Dolpa", stateId: State.KarnaliPradesh},
    {id: District.Humla, name: "Humla", stateId: State.KarnaliPradesh},
    {id: District.Jajarkot, name: "Jajarkot", stateId: State.KarnaliPradesh},
    {id: District.Jumla, name: "Jumla", stateId: State.KarnaliPradesh},
    {id: District.Kalikot, name: "Kalikot", stateId: State.KarnaliPradesh},
    {id: District.Mugu, name: "Mugu", stateId: State.KarnaliPradesh},
    {id: District.Salyan, name: "Salyan", stateId: State.KarnaliPradesh},
    {id: District.Surkhet, name: "Surkhet", stateId: State.KarnaliPradesh},
    {id: District.WesternRukum, name: "Western Rukum", stateId: State.KarnaliPradesh},

     // State 7: Sudurpashchim
     {id: District.Achham, name: "Achham", stateId: State.SudurpaschimPradesh},
     {id: District.Baitadi, name: "Baitadi", stateId: State.SudurpaschimPradesh},
     {id: District.Bajhang, name: "Bajhang", stateId: State.SudurpaschimPradesh},
     {id: District.Bajura, name: "Bajura", stateId: State.SudurpaschimPradesh},
     {id: District.Dadeldhura, name: "Dadeldhura", stateId: State.SudurpaschimPradesh},
     {id: District.Darchula, name: "Darchula", stateId: State.SudurpaschimPradesh},
     {id: District.Doti, name: "Doti", stateId: State.SudurpaschimPradesh},
     {id: District.Kailali, name: "Kailali", stateId: State.SudurpaschimPradesh},
     {id: District.Kanchanpur, name: "Kanchanpur", stateId: State.SudurpaschimPradesh},
    
  ];

}
