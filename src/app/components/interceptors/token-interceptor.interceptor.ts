import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { CustomerAuthService } from 'src/app/services/customer-auth.service';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private _authService: CustomerAuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const jwtToken = localStorage.getItem("jwtToken"); 
    if (jwtToken) {
      const requestWithJwtToken = request.clone({
        headers: request.headers.set("Authorization", "Bearer " + jwtToken)
      });
      return next.handle(requestWithJwtToken)
    }
    else {
      localStorage.removeItem("jwtToken"); 
      return next.handle(request);
    }
  }
}
