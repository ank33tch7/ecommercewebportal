import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustomerAccountService, Gender } from 'src/app/services/web-api-client';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit{
  //for reactive form
  registerForm: any;
  submitted: boolean = false;

  genders : any = [
    {id : Gender.Male, name: "Male"},
    {id : Gender.Female, name: "Female"}
  ];

  constructor(private _formBuilder: FormBuilder,
            private _customerAccountService: CustomerAccountService,
            private _toastrService: ToastrService,
            private _router: Router){}
  ngOnInit(): void {
    this.registerForm = this._formBuilder.group({
      fullName : [null, Validators.required],
      email: [null, Validators.required],
      phoneNumber: [null, Validators.required],
      gender: [null, Validators.required],
      address: [null,Validators.required],
      dateOfBirth : [null, Validators.required],
      password: [null, Validators.required],
      confirmPassword :  [null, Validators.required],
      image: [null],
    });
  }

  get getFormControl(){
    return this.registerForm.controls;
  }
  onRegisterCustomer() : void{
    this.submitted = true;
    if(this.registerForm.invalid) return;
    this._customerAccountService.createCustomer(this.registerForm.value).subscribe(res =>{
      this._toastrService.success("User is created successfully. Please proceed to login.");
      this._router.navigate(["/login"]);
    },err =>{
      this._toastrService.error("Unable to create user.");
    })
  }

}
