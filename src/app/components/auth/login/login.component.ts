import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustomerAuthService } from 'src/app/services/customer-auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  //for reactive form
  loginForm: any;
  submitted: boolean = false;

  constructor(private _formBuilder: FormBuilder,
              private _authService: CustomerAuthService,
              private _toastrService: ToastrService,
              private _router : Router){}
  ngOnInit(): void {
    this.loginForm = this._formBuilder.group({
      userName : [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  get getFormControl(){
    return this.loginForm.controls;
  }
  
  onLogin(): void{
    this.submitted = true;
    if(this.loginForm.invalid) return;

    this._authService.login(this.loginForm.value).subscribe(res =>{
      this._toastrService.success("You are logged in successfully.")
      this._router.navigate(["/"]);
    },err=>{
      this._toastrService.error("Login failed. Unauthorized access.")
    })
    
  }
}
