import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AddToCartService } from 'src/app/services/add-to-cart.service';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent implements OnInit {
  productCount: any;
  constructor(private _cartService: AddToCartService) {
    
  }

  ngOnInit() : void {
    this._cartService.productCount$.subscribe(res =>{
      this.productCount = res;
    });
  }
}
