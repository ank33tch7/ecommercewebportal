import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CustomerAuthService } from 'src/app/services/customer-auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isLoggedIn$ : Observable<boolean>;

  isTokenExpired: boolean  = false;

  constructor(private _authService: CustomerAuthService){
    this.isLoggedIn$ = this._authService.isLoggedIn$;
  }
  ngOnInit(): void {

   
  }

  getUsername() : string {
    return this._authService?.userInfo?.fullName ?? "";
  }

  onLogout() : void{
    this._authService.logout();
  }
}
