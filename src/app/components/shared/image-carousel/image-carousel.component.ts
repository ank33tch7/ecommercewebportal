import { Component, Input, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-image-carousel',
  templateUrl: './image-carousel.component.html',
  styleUrls: ['./image-carousel.component.css']
})
export class ImageCarouselComponent implements OnInit, OnDestroy {
  @Input("images") images: any;
  currentIndex = 0;
  carouselWidth = 750; // Adjust the desired width
  transformValue = 0;
  carouselHeight = 320;
  slideInterval = 5000;
  private interval: any;

  ngOnInit() {
    this.startSlideShow();
  }

  ngOnDestroy() {
    this.stopSlideShow();
  }

  prevImage() {
    this.currentIndex = (this.currentIndex - 1 + this.images.length) % this.images.length;
    this.updateTransformValue();
    this.stopSlideShow();
    this.startSlideShow();
  }

  nextImage() {
    this.currentIndex = (this.currentIndex + 1) % this.images.length;
    this.updateTransformValue();
    this.stopSlideShow();
    this.startSlideShow();
  }

  updateTransformValue() {
    this.transformValue = -this.currentIndex * this.carouselWidth;
  }

  startSlideShow() {
    this.interval = setInterval(() => {
      this.currentIndex = (this.currentIndex + 1) % this.images.length;
      this.updateTransformValue();
    }, this.slideInterval);
  }

  stopSlideShow() {
    clearInterval(this.interval);
  }

}