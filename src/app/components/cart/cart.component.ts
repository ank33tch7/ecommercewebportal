import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AddToCartModel } from 'src/app/models/add-to-cart.model';
import { AddToCartService } from 'src/app/services/add-to-cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cartDetails: any = null;
  // orderDetailCommand : OrderDetail[];
  constructor(private _cartService: AddToCartService,
            private _toastrService: ToastrService,
            private _router: Router) {

  }
  ngOnInit(): void {
    this.loadCartData();
  }

  loadCartData() : void {
    this._cartService.cartData.subscribe((res: AddToCartModel[]) => {
      this.cartDetails = res;
    });
  }
  getCartTotal() : number {
    let total =0;
    for(let i=0;i< this.cartDetails.length; i++){
      total += this.cartDetails[i].totalAmount;
    }
    return total;
  }

  removeFromCart(id : string) : void{
    if(this._cartService.checkIfExistsProductInCart(id)){
        this._cartService.removeProduct(id);
        this.loadCartData();
    }
  }

  checkIfAnyProductExists() : void {
     this._cartService.productCount$.subscribe(productCount =>{
      if(productCount <=0 ){
        this._toastrService.info("Your cart is empty. Please add a product to cart to proceed");
        
      }else{
        this._router.navigate(['/checkout']); 
      }
    });
  }
}

