import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { CartComponent } from './components/cart/cart.component';
import { HomeComponent } from './components/home/home.component';
import { ShopComponent } from './components/shop/shop.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { ProductDetailComponent } from './components/product/product-detail/product-detail.component';
import { AuthGuard } from './components/auth-guard/auth-guard.component';
import { OrderHistoryComponent } from './components/order-history/order-history.component';

const routes: Routes = [
  { path: "login", component: LoginComponent, data: { title: "Login" } },
  { path: "", component: HomeComponent,data: { title: "Home" } },
  { path: "register", component: RegisterComponent, data: { title: "Register" } },
  { path: "cart", component: CartComponent,canActivate: [AuthGuard], data: { title: "Cart Details" } },
  { path: "shop", component: ShopComponent, data: { title: "Product List" } },
  { path: "checkout", component: CheckoutComponent, canActivate: [AuthGuard], data: { title: "Checkout" } },
  { path: "product-detail/:id", component: ProductDetailComponent, data: { title: "Product Detail" } },
  { path: "order-history", component: OrderHistoryComponent,canActivate: [AuthGuard], data: { title: "Order History" } },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
