import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { OrderDetail } from './order-detail';
@Injectable({
    providedIn: 'root'
})
export class BuyNowService {
    private buyNow: OrderDetail[] = [];
    private _content$ = new BehaviorSubject<OrderDetail[]>(this.buyNow);
    buyNowData = this._content$.asObservable();
    orderDetailCommand: OrderDetail[] = [];
    constructor() {

    }

    onAddBuyProduct(orderDetail : OrderDetail) : void {
        this.buyNow.push(orderDetail);
        this._content$.next(this.buyNow);
    }

    getOrderDetail(): OrderDetail[] {
        let orderDetail = new OrderDetail();
        for (let i = 0; i < this.buyNow.length; i++) {
            orderDetail.price = this.buyNow[i].price;
            orderDetail.productId = this.buyNow[i].productId;
            orderDetail.quantity = this.buyNow[i].quantity;
            this.orderDetailCommand.push(orderDetail);
        }
       
        return this.orderDetailCommand;
    }

}