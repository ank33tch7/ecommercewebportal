import { Injectable } from '@angular/core';
import { AddToCartModel } from '../models/add-to-cart.model';
import { BehaviorSubject } from 'rxjs';
import { OrderDetail } from './order-detail';
@Injectable({
  providedIn: 'root'
})
export class AddToCartService {
  private addToCart: AddToCartModel[] = [];
  private _content$ = new BehaviorSubject<AddToCartModel[]>(this.addToCart);
  cartData = this._content$.asObservable();

  private productCot: number = 0;
  private _productCount$ = new BehaviorSubject<number>(this.productCot);
  productCount$ = this._productCount$.asObservable();

  orderDetailCommand: OrderDetail[] = [];

  constructor() {

  }

  onAddProductDetails(cartServiceData: AddToCartModel) : void {
    this.addToCart.push(cartServiceData);
    this._content$.next(this.addToCart);
  }

  onCountProduct() : void{
    this.productCot += 1;
    this._productCount$.next(this.productCot);
  }

  onRemoveCountProduct() : void {
    this.productCot -= 1;
    this._productCount$.next(this.productCot);
  }

  getTotalAmount(): number {
    let amount = 0;
    for (let i = 0; i < this.addToCart.length; i++) {
      amount += this.addToCart[i]?.unitPrice * this.addToCart[i]?.quantity;
    }
    return amount;
  }

  getOrderDetail(): OrderDetail[] {
    if (this.addToCart.length < 1) {
      return this.orderDetailCommand;
    }
    for (let i = 0; i < this.addToCart.length; i++) {
      let orderDetail = new OrderDetail();
      orderDetail.price = this.addToCart[i]?.unitPrice;
      orderDetail.productId = this.addToCart[i]?.id;
      orderDetail.quantity = this.addToCart[i]?.quantity;
      this.orderDetailCommand.push(orderDetail);
    }
    return this.orderDetailCommand;
  }

  getProductQuantityById(id: string) : number{
    const item = this.addToCart.find((item: any) => item.id === id);
  
  if (item) {
    return item.quantity;
  }
  
  // Item not found, handle the case accordingly
  // Option 1: Return a default value (e.g., 0)
  return 0;

  // Option 2: Throw an error indicating the item was not found
  // throw new Error(`Item with ID '${id}' not found.`);
  }


  checkIfExistsProductInCart(id: string): boolean {
   return this.addToCart.some((item: any) => item.id === id);  
  }

  removeProduct(id: string): void {
    this.addToCart = this.addToCart.filter(item => item.id !== id);
    this.onRemoveCountProduct();
    this._content$.next(this.addToCart);
  }

  removeAllProductFromCart(){
    this.addToCart = [];
    this.productCot = 0;
    this._productCount$.next(this.productCot);
    this._content$.next(this.addToCart);
  }

}
