import { OrderDetailCommand } from "./web-api-client";

export class OrderDetail implements OrderDetailCommand {
    price?: number;
    quantity?: number;
    productId?: string;
  }