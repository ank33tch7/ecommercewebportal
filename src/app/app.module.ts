import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule , NoopAnimationsModule} from '@angular/platform-browser/animations';
// Import your library
import { SlickCarouselModule } from 'ngx-slick-carousel';

import { NgSelectModule } from '@ng-select/ng-select';
import { ToastrModule } from 'ngx-toastr';
// import { ScrollingModule } from '@angular/cdk/scrolling';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { CartComponent } from './components/cart/cart.component';
import { ProductByCategoryComponent } from './components/product/product-by-category/product-by-category.component';
import { ProductByBrandsComponent } from './components/product/product-by-brands/product-by-brands.component';
import { MainMenuComponent } from './components/shared/main-menu/main-menu.component';
import { HeaderBrandComponent } from './components/shared/header-brand/header-brand.component';
import { ProductSliderComponent } from './components/product/product-slider/product-slider.component';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { ProductDetailComponent } from './components/product/product-detail/product-detail.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { ShopComponent } from './components/shop/shop.component';
import { CustomerAuthService } from './services/customer-auth.service';

import { AuthGuard } from './components/auth-guard/auth-guard.component';
import { TokenInterceptorService } from './components/interceptors/token-interceptor.interceptor';
import { DatePickerComponent } from './components/date-picker/date-picker.component';
import { LastestProductComponent } from './components/product/lastest-product/lastest-product.component';
import { ShopPromoComponent } from './components/shop-promo/shop-promo.component';
import { FooterTopComponent } from './components/shared/footer-top/footer-top.component';
import { AddToCartService } from './services/add-to-cart.service';
import { ImageCarouselComponent } from './components/shared/image-carousel/image-carousel.component';
import { OrderHistoryComponent } from './components/order-history/order-history.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    CartComponent,
    ProductByCategoryComponent,
    ProductByBrandsComponent,
    MainMenuComponent,
    HeaderBrandComponent,
    ProductSliderComponent,
    LoginComponent,
    RegisterComponent,
    ProductDetailComponent,
    CheckoutComponent,
    ShopComponent,
    DatePickerComponent,
    LastestProductComponent,
    ShopPromoComponent,
    FooterTopComponent,
    ImageCarouselComponent,
    OrderHistoryComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    NoopAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgSelectModule,
    ToastrModule.forRoot({
      timeOut: 4000,
      // closeButton: true,
      progressBar: true,
      tapToDismiss: true,
      preventDuplicates: true,
      countDuplicates: false,
      easeTime: 800,
      positionClass: 'toast-bottom-right'
    }) ,
    FormsModule,
    SlickCarouselModule
  ],
  
  providers: [
    CustomerAuthService,
    AddToCartService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
