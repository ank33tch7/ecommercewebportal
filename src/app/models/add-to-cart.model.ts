export class AddToCartModel{
    id : string ;
    productName : string;
    brandName: string;
    unitPrice: number = 0;
    quantity: number = 0;
    totalAmount : number = 0;
}