export const  State = {
    KoshiPradesh : "Koshi Pradesh",
    MadeshPradesh : "Madesh Pradesh",
    BagmatiPradesh: "Bagmati Pradesh",
    GandakiPradesh : "Gandaki Pradesh",
    LumbiniPradesh : "Lumbini Pradesh",
    KarnaliPradesh : "Karnali Pradesh",
    SudurpaschimPradesh : "Sudurpaschim Pradesh"
}