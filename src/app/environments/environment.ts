export const environment = {
    production: false,
    deploymentType:"UAT",
    imageUrl: 'https://localhost:7026/images/',
    apiUrl : 'https://localhost:7026/'
  };